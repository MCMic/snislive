#!/usr/bin/env bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e -u

# Warning: customize_airootfs.sh is deprecated! Support for it will be removed in a future archiso version.

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

# Remove local mirror used for snis installation
sed -i 's/\(.snis.\)/#\1/' /etc/pacman.conf
sed -i 's/\(SigLevel = Optional TrustAll\)/#\1/' /etc/pacman.conf
sed -i 's|\(Server = file:///tmp/snis\)|#\1|' /etc/pacman.conf

systemctl enable pacman-init.service choose-mirror.service lxdm.service NetworkManager.service
systemctl set-default graphical.target
