live: /tmp/snis/snis.db.tar.gz
	( cd snislive ; sudo mkarchiso -v -w work -o out . )

/tmp/snis/snis.db.tar.gz: snis-git
	( cd snis-git ; makepkg -f )
	mkdir -p /tmp/snis
	repo-add /tmp/snis/snis.db.tar.gz ./snis-git/*.pkg.*
	cp ./snis-git/*.pkg.* /tmp/snis/

snis-git:
	git clone https://aur.archlinux.org/snis-git.git

upload-iso:
	echo -e "mput snislive/out/*.iso www/\nexit\n" | sftp -i id_upload -P 22222 come@mugoreve.fr

clean:
	sudo rm -rf snislive/out
	sudo rm -rf snislive/work
	rm -rf /tmp/snis
	rm -rf snis-git
