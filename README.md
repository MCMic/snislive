# Space Nerds In Space Live USB

This is an attempt at building live ISO for playing Space Nerds In Space.

It’s based on the archiso tool, and thus is only possible on 64bit architecture.

## Download

You can download the latest build of the ISO file at this URL: https://snis.mugoreve.fr/

## Use

To use the ISO you should burn it to a USB stick.

More information here: https://wiki.archlinux.org/index.php/USB_flash_installation_media#Using_automatic_tools

I tested the ISO using dd and it worked fine. Be careful not to erase your harddrive, double-check the target device of the command.

## Details

As of now the system looks like this:

1. ArchLinux base
2. LXDM
3. LXDE desktop
4. Network Manager
5. Space Nerds In Space of course (using the AUR package: https://aur.archlinux.org/packages/snis-git)

You can use the Makefile to build everything.

This is possible thanks to archiso: https://wiki.archlinux.org/index.php/archiso

And you can find more information on Space Nerds In Space here: https://smcameron.github.io/space-nerds-in-space/